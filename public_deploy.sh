#!/bin/sh

# public_deploy.sh
#
# for use by Gitlab CI/CD config
# Entire site is already built for deployment in _deploy before CI/CD.
#
# Rename assets by suffixing file names with hash code based on their content,
# except index.html which are not renamed.
# Several previous assets with suffix attached are included in case
# old running or cached web pages still refer to the old versions. The
# Gitlab CI/CD config uses a cache for the save-assets directory to do this.
#
# Create a hash code file for each HTML file not in assets. Edit the call
# to CMKSetupReload in each HTML file to have the name of the hash code file
# and the loaded hash value. The CWKReloadPage function checks the file code
# vesus the loaded code, and reloads if changed, hopefully bypassing the cache.
#
# Create build.txt with the time since the epoch in seconds. This is
# read the first time CWKReloadPage is called. For later calls, if the build
# time does not change, then the HTML hash code files are not read.
# This way only one file is being read most of the time, to reduce the work
# on the server. At some point this file might be stored on a separate server
# that is not optimized for static site serving. The time in rebuild.txt
# is used to increase the time between CWKReloadPage checks when there has
# been no new updates to the site for a while. Any parameters to this script
# are appended one per line to this file.

ME="`basename "$0"`"

rm -rf public
cp -r _deploy public

# save assets in save-assets directory, which is cached by Gitlab CI/CD
[ -d save-assets ] || mkdir save-assets

# do not change the build date unless the arguments change, unless no arguments
if [ $# -eq 0 ]; then
  date +%s > public/build.txt
else
  echo -n > new-args
  for i in "$@"
  do
    echo "$i" >> new-args
  done
  if ! cmp -s new-args save-assets/old-args; then
    date +%s > save-assets/build.txt
    cat new-args >> save-assets/build.txt
    mv new-args save-assets/old-args
  else
    rm -rf new-args
    echo 'Short SHA did not change, using old build.txt:'
    cat save-assets/build.txt
  fi
  cp save-assets/build.txt public/build.txt
fi

# rename all current assets to have hash code suffix
FSUB=''
for i in `cd public; find assets ! -type d ! -name index.html`
do
  code=`md5sum public/$i | sed 's/^\(........\).*$/\1/'`
  newfile="`echo "$i" | sed "s/\\.[^.]*\$/-$code&/"`"
  mv "public/$i" "public/$newfile" -v
  oldfile="`echo "$i" | sed 's,/,\\\\/,g'`"
  newfile="`echo "$newfile" | sed 's,/,\\\\/,g'`"
  if [ -n "$FSUB" ]; then FSUB="$FSUB;"; fi
  FSUB="${FSUB}s/$oldfile\"/$newfile\"/"
done

# cause all html files to use assets with hash code suffix
find public -name \*.html -execdir grep -q -s assets {} \; -print0 |
    xargs -0 -n 5 -P 3   sed -i -e "$FSUB"
# the above could be written:
#sed -i -e "$FSUB" `find public -name \*.html -execdir grep -q -s assets {} \;`
# if xargs is not available. xargs is set to run 3 seds in parallel and it
# allows an unlimited number of html files, but that is not necessary here.

# write hash code file for all HTML files not in assets directory
find public -path public/assets -prune -o -name \*.html -print |
    sed 's/\.[^.]*$//;s/^public//;s:.*:md5sum public&.html | sed '\''s/^\\(........\\).*$/\\1/'\'' >public&-hash.txt ; sed -i -e "s!CWKSetupReload( *null *, *null *)!CWKSetupReload('\''&-hash.txt'\'', '\''"`cat public&-hash.txt`"'\'')!" public&.html:' |
    /bin/sh

# move the current assets to save-assets/1
# it should not already exist, just in case:
rm -rf save-assets/1
mv public/assets save-assets/1

echo save-assets before deletions:
ls -l save-assets

# delete anything in save-assets/2 which exists in save-assets/1
[ -d save-assets/2 ] && ( cd save-assets/2 ; exec find . ! -type d |
  sed 's:.*:[ -e '\''../1/&'\'' ] \&\& rm '\''&'\'':' |
  /bin/sh )

# remove any assets copy which has not been used in more than 7 days;
# this depends on the above commands deleting index.html in save-assets/2,
# setting the directory's modification time to the current time,
# which is the time save-assets/2 stops being used
find save-assets -type d -maxdepth 1 -mtime +7 -print0 | xargs -0 rm -rf

# remove empty save-assets subdirectories
find save-assets -type d -empty -delete

# copy all the saved assets to public in one shot
mkdir public/assets
( cd save-assets; cp -r */* ../public/assets -v )

# move save-assets subdirectories up, 1 is current before move, higher is older
# temp dirs work-assets and tmp-save should not already exist, rm just in case
rm -rf work-assets
mkdir work-assets
[ -e save-assets/build.txt ] && mv save-assets/build.txt work-assets
[ -e save-assets/old-args ] && mv save-assets/old-args work-assets
rm -rf tmp-save
# assets are saved for each CI/CD run, with one copy saved per each iteraion
# of the following for loop, except that duplicates are removed;
# the first copy will be created from the current assets at the next deployment
for i in 1 2 3 4 5 6 7 8 9 10
do
  [ -d tmp-save ] && mv tmp-save work-assets/$i
  [ -d save-assets/$i ] && mv save-assets/$i tmp-save
done
rm -rf tmp-save
rm -rf save-assets
mv work-assets save-assets
